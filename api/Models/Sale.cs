using System.ComponentModel.DataAnnotations;
using api.Enums;

namespace api.Models
{
    public class Sale
    {
        public int Id{get; set; }
        public int SalesmanId {get; set; }
        public List<Item>Items{get; set; }
        public SaleStatus Status{get; set; }
    }
}