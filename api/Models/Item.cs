using System.ComponentModel.DataAnnotations;

namespace api.Models
{
    public class Item
    {
        public int Id;
        public string Name;
        public string Description;
        public decimal Price;
        public int Quantity;
    }
}