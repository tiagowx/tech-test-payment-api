using Microsoft.EntityFrameworkCore;
using api.Models;

namespace api.Context
{
    public class ApiContext: DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
            
        }

        public DbSet<Sale> Sales { get; set; }
        public DbSet<Item> Items { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Sale>().HasKey(k => k.Id);
            modelBuilder.Entity<Item>().HasKey(k => k.Id);
        }
    }


}