namespace api.Enums
{
    public enum SaleStatus
    {
        Cancelada, 
        Aguardando,
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
    }
}