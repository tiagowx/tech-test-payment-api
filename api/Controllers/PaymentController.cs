using api.Context;
using api.Enums;
using api.Models;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly ApiContext _context;

        public  PaymentController(ApiContext context){
            _context = context;
        }    

        [HttpPost]
        public ActionResult CreateSale(int _salesmanId, List<Item> _items) {
            if(_items.Count == 0 || (_salesmanId <= 0)) {
                return BadRequest("Não existe itens no carrinho de compras.");
            } else {
                var newSale = new Sale{
                    SalesmanId = _salesmanId,
                    Items = _items,
                    Status = SaleStatus.Aguardando
                };

                _context.Sales.Add(newSale);
                _context.SaveChanges();
                
                return Ok();
            }

        }

        [HttpGet]
        public ActionResult GetSaleAll() {
            
            var sale = _context.Sales;

            if(sale == null){
                return NotFound(sale);
            }

            return Ok(sale);
        }

        [HttpGet("{id}")]
        public ActionResult GetSaleById(int id) {
            
            var sale = _context.Sales.Find(id);

            if(sale == null){
                return NotFound(sale);
            }

            return Ok(sale);
        }

        [HttpPut("{id}")]
        public ActionResult UpgradeSaleStatus(int id){
            var changedSale = _context.Sales.Find(id);
            
            if (changedSale == null) {
                return NotFound("Venda não encontrada.");
            } 

            changedSale.Status++;
            
            _context.SaveChanges();

            return Ok(changedSale);
        }
    }
}